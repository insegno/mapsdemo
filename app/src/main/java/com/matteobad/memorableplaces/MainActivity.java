package com.matteobad.memorableplaces;

import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    static ArrayList<String> places;
    static ArrayList<LatLng> locations;
    static ArrayAdapter<String> arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView = (ListView) findViewById(R.id.placesListView);
        SharedPreferences sharedPreferences = this.getSharedPreferences(getPackageName(), MODE_PRIVATE);

        if (sharedPreferences.contains("places") && sharedPreferences.contains("latitudes") && sharedPreferences.contains("longitudes")) {

            try {
                String serializedPlaces = sharedPreferences.getString("places", ObjectSerializer.serialize(new ArrayList<String>()));
                String serializedLatitudes = sharedPreferences.getString("latitudes", "");
                String serializedLongitudes = sharedPreferences.getString("longitudes", "");

                places = (ArrayList<String>) ObjectSerializer.deserialize(serializedPlaces);
                ArrayList<String> latitudes = (ArrayList<String>) ObjectSerializer.deserialize(serializedLatitudes);
                ArrayList<String> longitudes = (ArrayList<String>) ObjectSerializer.deserialize(serializedLongitudes);

                locations = new ArrayList<>();
                for (int i = 0; i < latitudes.size() && i < longitudes.size(); i++) {
                    locations.add(new LatLng(Double.valueOf(latitudes.get(i)), Double.valueOf(longitudes.get(i))));
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            places = new ArrayList<>();
            locations = new ArrayList<>();
            places.add("Add a new place...");
            locations.add(new LatLng(0, 0));
        }

        arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, places);

        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                intent.putExtra("placeNumber", position);
                startActivity(intent);
            }
        });

    }
}
